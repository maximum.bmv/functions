const getSum = (str1, str2) => {
  // add your implementation below
  if (isNaN(+str1) || isNaN(+str2)){
    return false
  } else if (typeof str1 !== 'string' || typeof str2 !== 'string'){
    return false
  } else {
    if (str1 === ''){
      str1 = '0'
    } else if (str2 === '') {
      str2 = '0';
    }
    let newArr = [];
    let arr1 = str1.split('');
    let arr2 = str2.split('');
    if (arr1.length >= arr2.length){
      for (let i = 0; i < arr1.length; i++){
        if (arr2[i]){
          newArr.push(+arr1[i] + +arr2[i])
        } else {
          newArr.push(+arr1[i]);
        }         
      }
    } else {
      for (let i = 0; i < arr2.length; i++){
        if (arr1[i]){
          newArr.push(+arr2[i] + +arr1[i])
        } else {
          newArr.push(+arr2[i]);
        }         
      }
    }   
    let newStr = newArr.join('')
    return newStr;
  }
};
const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  // add your implementation below
  let commentsAuthor = 0;
  let posts = 0;
  for (let i = 0; i < listOfPosts.length; i++){
    for (let key in listOfPosts[i]){
      switch (listOfPosts[i][key]){
        case authorName :
          posts++
          break;
      }
      if(key === 'comments'){
        let comments = listOfPosts[i][key];
          for (let j = 0; j < comments.length; j++){
            for (let prop in comments[j]){
              switch (comments[j][prop]){
                case authorName :
                  commentsAuthor++
              }
            }
          }  
      }
    }
  }
  return `Post:${posts},comments:${commentsAuthor}`;
};
const tickets=(people)=> {
  // add your implementation below
  let myChange = [];
  people = people.map(item => {
    item = +item;
    switch (item) {
      case 25:
        myChange.push(item);
        return true;
      case 50:
        let index = myChange.indexOf(25);
        if (index !== -1){
          myChange[index] = item;
        }
        return true;
      case 100:
        let index25 = myChange.indexOf(25);
        let index50 = myChange.indexOf(50);
        if (index50 !== -1 && index25 !== -1){
          myChange[index50] = item;
          myChange = myChange.splice(index25, 1);
          return true;
        } else {
          let myArr = [];
          for (let i = 0; i < myChange.length; i++){
            if(myChange[i] === 25){
              myArr.push(i);
            }
          }
          if (myArr.length >= 3){
            for (let i = 0; i < 3; i++){
              myChange.splice(myArr[i], 1);
            }
            return true
          } else {
            return false
          }
        } 
    }
  })
  let result = 'YES';
  for (let i = 0; i < people.length; i++){
    if (!people[i]){
      result = 'NO'
    } 
  }  
  return result;
};
module.exports = {getSum, getQuantityPostsByAuthor, tickets};
